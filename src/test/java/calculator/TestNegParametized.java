package calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class TestNegParametized {

    @Parameters
    public static Collection<Integer[]> parameters() {
        return Arrays.asList(new Integer[][]{
                {-2, 4, 16},
                {-4, 0, 4},
                {-2, 6, 15},
                {7, 2, 49}
        });
    }

    @Parameterized.Parameter(0)
    public  int a ;


    @Parameterized.Parameter(1)
    public  int b ;

    @Parameterized.Parameter(2)
    public int expectedResult;

    @Test
    public void powerOfN() {

        Calculator calculator = new Calculator();
        int result = calculator.powerOfN(a,b);
        assertEquals(expectedResult,result);
    }
}


