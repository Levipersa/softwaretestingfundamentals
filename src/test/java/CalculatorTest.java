import calculator.Calculator;
import org.junit.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CalculatorTest {
    private Calculator calculator = new Calculator();

    @BeforeClass
    public void setup(){
        System.out.println("Before each test");
    }
    @AfterClass
    public void tearDown(){
        System.out.println("After each test ");
    }

    @Test
    public void test_add() {
        //given
        //when
        int result = calculator.add(4, 4);
        //then

//       assert result == 8;
        assertEquals(8, result);

        int result2 = calculator.add(-1, -3);

        assertNotNull(result2);

    }

    @Test
    public void test_divide_succes() {
        int result = calculator.divide(4, 4);
        assertNotNull(result);
        //    mesajul este afisat  cand metoda nu functioneaza corect
        assertEquals("Don't work as expected", 1, result);


    }
    @Test(expected = ArithmeticException.class)
    public void test_divide_thorws_exception() {
        int result2 = calculator.divide(4, 0);
        assertNotNull(result2);
    }

    @Test
    public void test_powerOfN_succes() {
        int result = calculator.powerOfN(2,4);
        assertEquals(16,result);
    }
}
